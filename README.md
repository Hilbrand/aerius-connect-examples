Dit project bevat een aantal programmeer voorbeelden voor het gebruik van AERIUS Connect. Deze voorbeelden zijn puur ter illustratie van het gebruik van de AERIUS Connect API.

**Let op! Vanaf API v3 is websocket vervangen door HTTP REST als communicatie protocol.**

Op dit moment zijn er voorbeelden in de volgende programmeertalen:

## python:
- python 2 of 3.
- module arrow: <code>pip install arrow</code>
- module bravado: <code>pip install bravado</code>
